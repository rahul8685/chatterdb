//
// Created by rahul on 3/21/16.
//

#ifndef CHATTERDB_WEBSOCKET_H
#define CHATTERDB_WEBSOCKET_H

#include <list>
#include <memory>
#include <iostream>
#include <array>
#include <utility>

#include <boost/asio/io_service.hpp>
#include <boost/asio/buffer.hpp>
#include <boost/asio/ip/tcp.hpp>
#include "HTTPParser.h"
#include "cutils.h"


class WebSocket;


/*
 0                   1                   2                   3
 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-------+-+-------------+-------------------------------+
|F|R|R|R| opcode|M| Payload len |    Extended payload length    |
|I|S|S|S|  (4)  |A|     (7)     |             (16/64)           |
|N|V|V|V|       |S|             |   (if payload len==126/127)   |
| |1|2|3|       |K|             |                               |
+-+-+-+-+-------+-+-------------+ - - - - - - - - - - - - - - - +
|     Extended payload length continued, if payload len == 127  |
+ - - - - - - - - - - - - - - - +-------------------------------+
|                               |Masking-key, if MASK set to 1  |
+-------------------------------+-------------------------------+
| Masking-key (continued)       |          Payload Data         |
+-------------------------------- - - - - - - - - - - - - - - - +
:                     Payload Data continued ...                :
+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +
|                     Payload Data continued ...                |
+---------------------------------------------------------------+
 */

enum class Opcode : uint8_t {
  continuation = 0x0,
  text = 0x1,
  binary = 0x2,
  connection_close = 0x8,
  ping = 0x9,
  pong = 0xA
};

template <class T>
class BufferRef {
 public:
  typedef typename T::iterator iterator;
  typedef typename T::const_iterator const_iterator;

  BufferRef(T& r) : ref(r) {}
  BufferRef(const BufferRef &) = default;

  T& ref;

  iterator begin() { return ref.begin(); }
  const_iterator begin() const { return ref.begin(); }
  iterator end() { return ref.end(); }
  const_iterator end() const { return ref.end(); }
};

template<class T>
class Handler {
 public:
  typedef void (*Pointer) (Handler<T> &, T &, const boost::system::error_code &, std::size_t);
  // std::function occupies 96 bytes
  // typedef std::function<void(Handler<T> &, T &, const boost::system::error_code &, std::size_t)> Pointer;

  Handler(std::unique_ptr<T> &&s) : state(std::move(s)) { }
  Handler(std::unique_ptr<T> &s) : state(std::move(s)) { }
  Handler(Handler &&) = default;
  Handler(const Handler &other);

  inline void operator()(const boost::system::error_code &ec, std::size_t bytes_transferred) {
    if (ec.value() == boost::asio::error::operation_aborted) {
      return;
    }
    state->cb(*this, *state, ec, bytes_transferred);
  }

  std::unique_ptr<T> state;
};

class HeaderData {
 public:
  HeaderData(WebSocket &ws, Handler<HeaderData>::Pointer p) : websocket(ws), cb(p) {}

  WebSocket &websocket;
  HTTPParser par;
  char cbuffer[64];
  Handler<HeaderData>::Pointer cb;
  static const char *response_header_start;
  static const char *response_header_end;
  char sec_websocket_accept[28];
};

class FrameData {
 public:
  typedef std::vector<uint8_t> Payload;
  FrameData(WebSocket &ws, Handler<FrameData>::Pointer p) : cb(p), websocket(ws), is_fragmented(false) {}
  Handler<FrameData>::Pointer cb;
  WebSocket &websocket;
  Payload payload;
  uint64_t length;
  uint8_t masking_key[4];
  uint8_t frame_start[2];
  bool is_fragmented;
};

class SendingData {
 public:
  SendingData(FrameData::Payload &pl) : payload(std::move(pl)) {}
  FrameData::Payload payload;
  uint64_t extended_length;
  uint8_t frame_start[2];
};

class BufferedSendData {
 public:
  //TODO: Remove the redundant unique_ptr
  typedef std::vector<std::unique_ptr<SendingData>> PayloadList;
  typedef std::vector<boost::asio::const_buffer> BufferList;

  BufferedSendData(PayloadList &pl, BufferList &bl, WebSocket &ws, Handler<BufferedSendData>::Pointer p, bool hc) : cb(p), websocket(ws), has_close(hc) {
    pl.swap(payload_list);
    pl.reserve(10);
    bl.swap(buffer_list);
    bl.reserve(10);
  }

  Handler<BufferedSendData>::Pointer cb;
  WebSocket &websocket;
  PayloadList payload_list;
  BufferList buffer_list;
  bool has_close;
};

class Acceptor;

typedef void (*PayloadCallback) (WebSocket &, FrameData::Payload &, Opcode type, bool is_final);
typedef void (*SentCallback) (WebSocket &, FrameData::Payload &);


class WebSocket {
 public:
  explicit WebSocket(boost::asio::io_service &io_service, Acceptor &ac) : socket(io_service), acceptor(ac), is_sending(false), close_requested(false) {}

  WebSocket(const WebSocket &) = delete;

  void start_receive();

  void send(FrameData::Payload &payload, Opcode opcode, bool is_final);
  void send_close(bool is_close_response, FrameData::Payload &payload);
  void send_close(bool is_close_response, std::uint16_t code, std::string reason);

  void send_buffered();
  void close();

  boost::asio::ip::tcp::socket socket;
  Acceptor &acceptor;
  std::list<WebSocket>::iterator it;
  BufferedSendData::PayloadList payload_list;
  BufferedSendData::BufferList buffer_list;
  bool is_sending;
  bool close_requested;
};

class Acceptor: boost::noncopyable {
 public:
  Acceptor(boost::asio::io_service &ios, PayloadCallback pc, SentCallback sc) :
      io_service(ios),
      acceptor(ios, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), 9001)),
      payload_callback(pc),
      sent_callback(sc)  {}

  void start_accept();

  std::list<WebSocket> socket_list;
  boost::asio::io_service &io_service;
  boost::asio::ip::tcp::acceptor acceptor;
  PayloadCallback payload_callback;
  SentCallback sent_callback;
};



#endif //CHATTERDB_WEBSOCKET_H
