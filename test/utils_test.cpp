#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wglobal-constructors"

#include "gtest/gtest.h"
#include <iostream>
#include <list>
#include <boost/array.hpp>
#include "cutils.h"

using namespace std;


TEST(Utils, derive_sec_websocket_accept)
{
    boost::array<char, 28> sec_websocket_accept;
    derive_sec_websocket_accept("dGhlIHNhbXBsZSBub25jZQ==", sizeof("dGhlIHNhbXBsZSBub25jZQ==") - 1, sec_websocket_accept.data());

    string accept(sec_websocket_accept.c_array(), sec_websocket_accept.size());
    EXPECT_TRUE(accept == "s3pPLMBiTxaQ9kYGzzhZRbK+xOo=");
}

#pragma clang diagnostic pop
