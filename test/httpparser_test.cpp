#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wglobal-constructors"

#include "gtest/gtest.h"
#include <string>
#include <list>
#include <iostream>
#include <HTTPParser.h>

using namespace std;

inline list<string> chunk_string(string &s, std::size_t chunk_size) {
    list<string> chunks;
    std::size_t size = s.size();
    for(std::size_t i=0; i<size; i=i+chunk_size) {
        chunks.push_back(s.substr(i, chunk_size));
    }
    return chunks;
}

inline bool compare_str_list(list<string> &l1, list<string> &l2) {
    if (l1.size() != l2.size()) {
        return false;
    }

    auto l1i = l1.begin();
    for(auto l2i = l2.begin(); l2i != l2.end(); l2i++, l1i++) {
        if((*l2i) != (*l1i)) {
            return false;
        }
    }

    return true;
}

inline void push_chunks(HTTPParser &par, list<string> &chunks) {
    for(list<string>::iterator it=chunks.begin(); it!=chunks.end(); ++it) {
        par.received_data(it->c_str(), it->size());
    }
}

TEST(compare_str_list, test1)
{
    std::list<std::string> s1 { "red", "green", "blue" };
    std::list<std::string> s2 { "black", "blue", "white", "green" };

    EXPECT_FALSE(compare_str_list(s1, s2));

    std::list<std::string> s3 { "red", "green", "blue" };
    std::list<std::string> s4 { "red", "green", "blue" };

    EXPECT_TRUE(compare_str_list(s3, s4));
}

TEST(chunk_string, test1)
{
    string s("redgrebl");
    std::list<std::string> sl{"red", "gre", "bl" };
    auto cs = chunk_string(s, 3);
    EXPECT_TRUE(compare_str_list(cs, sl));

    s = "1234567";
    sl = {"1234", "567"};
    cs = chunk_string(s, 4);
    EXPECT_TRUE(compare_str_list(cs, sl));

    s = "12345678";
    sl = {"1234", "5678"};
    cs = chunk_string(s, 4);
    EXPECT_TRUE(compare_str_list(cs, sl));

    s = "12345678";
    sl = {"12345678"};
    cs = chunk_string(s, 9);
    EXPECT_TRUE(compare_str_list(cs, sl));
}

TEST(HTTPParser, basic)
{
    const char* header = R"(GET /chat HTTP/1.1
Host: example.com:8000
Upgrade: cook
Connection: Upgrade
User-agent: Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36
Sec-WebSocket-Key: dGhlIHNhbXBsZSBub25jZQ==
Sec-WebSocket-Version: 13

)";

    HTTPParser par;
    par.received_data(header, strlen(header));

    EXPECT_EQ(par.user_agent, "Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36");
    EXPECT_EQ(par.header_complete, true);
    EXPECT_EQ(par.message_complete, true);
    EXPECT_EQ(par.url, "/chat");

    EXPECT_EQ(par.parser.http_major, 1);
    EXPECT_EQ(par.parser.http_minor, 1);
    EXPECT_EQ(par.parser.status_code, 0);
    EXPECT_STREQ(http_method_str(static_cast<enum http_method>(par.parser.method)), "GET");
    EXPECT_STREQ(http_errno_name(HTTP_PARSER_ERRNO(&par.parser)), "HPE_OK");

    EXPECT_EQ(par.parser.upgrade, 1);
}

TEST(HTTPParser, splitup_input)
{
    const char* header = R"(GET /demo HTTP/1.1
Upgrade: WebSocket
Connection: Upgrade
Host: example.com
Origin: http://example.com
User-Agent: Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36
WebSocket-Protocol: sample

)";
    string header_s = string(header);

    for(size_t c=1; c<(header_s.size() + 1); c++) {
        HTTPParser par;
        list<string> chunks = chunk_string(header_s, c);
        push_chunks(par, chunks);

        EXPECT_EQ(par.user_agent,
                  "Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36");
        EXPECT_EQ(par.header_complete, true);
        EXPECT_EQ(par.message_complete, true);
        EXPECT_EQ(par.url, "/demo");

        EXPECT_EQ(par.parser.http_major, 1);
        EXPECT_EQ(par.parser.http_minor, 1);
        EXPECT_EQ(par.parser.status_code, 0);
        EXPECT_STREQ(http_method_str(static_cast<enum http_method>(par.parser.method)), "GET");
        EXPECT_STREQ(http_errno_name(HTTP_PARSER_ERRNO(&par.parser)), "HPE_OK");

        EXPECT_EQ(par.parser.upgrade, 1);
    }
}

TEST(HTTPParser, FailParsingPost)
{

    const char* header = R"(POST /dsadsa/ HTTP/1.1
accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
accept-encoding: gzip, deflate
accept-language: en-US,en;q=0.8
cache-control: max-age=0
content-length: 1503
content-type: multipart/form-data; boundary=----WebKitFormBoundaryFuWj8OXBBzBB07di
cookie: __cfduid=d8c7df196c63029dee72f537b78b014d71459405420; ORG8020=3da2b83d-cd50-4ceb-b0f1-53e586dc8c4b; sessionid=d7210f004873a5be061b4106400b6881; csrftoken=yll9PI7ERQEX0NIy4cy4FON7M9to8WpQ
origin: https://www.dsadsadsa.com
referer: https://www.dsadsadsa.com/dsadsadsa
upgrade-insecure-requests: 1
user-agent: Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36

------WebKitFormBoundaryFuWj8OXBBzBB07di
Content-Disposition: form-data; name="csrfmiddlewaretoken"

yll9PI7ERQEX0NIy4cy4FON7M9to8WpQ
------WebKitFormBoundaryFuWj8OXBBzBB07di
Content-Disposition: form-data; name="short_name"

S. van 't Noordende (Sandervtn)
------WebKitFormBoundaryFuWj8OXBBzBB07di
Content-Disposition: form-data; name="phrase_text"

https://twitter.com/Sandervtn
------WebKitFormBoundaryFuWj8OXBBzBB07di
Content-Disposition: form-data; name="is_active")";
    HTTPParser par;
    EXPECT_THROW(par.received_data(header, strlen(header)), InvalidData);
}

TEST(HTTPParser, RandomCrap)
{

    const char* header = R"(Upgrade: WebSocket
Connection: Upgrade
Host: example.com
Origin: http://example.com
)";
    HTTPParser par;
    EXPECT_THROW(par.received_data(header, strlen(header)), ParserException);
}

TEST(HTTPParser, ExceptionOnDataafterCompleteHeader)
{
    const char* header = R"(GET /demo HTTP/1.1
Upgrade: WebSocket
Connection: Upgrade
Host: example.com
Origin: http://example.com
User-Agent: Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36
WebSocket-Protocol: sample

)";

    HTTPParser par;
    par.received_data(header, strlen(header));
    EXPECT_THROW(par.received_data("junk", 4), InvalidData);
}


#pragma clang diagnostic pop
