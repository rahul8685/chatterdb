//
// Created by rahul on 4/28/16.
//

#ifndef BBCHATTER_CUTILS_H
#define BBCHATTER_CUTILS_H

#include <cstdint>
#include <cstddef>
#include <boost/utility/string_ref.hpp>

bool derive_sec_websocket_accept(const char* key, std::size_t len, char* dest);
bool utf8_check_is_valid(const boost::string_ref& string);


#endif //BBCHATTER_CUTILS_H
