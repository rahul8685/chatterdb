//
// Created by rahul on 4/28/16.
//

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wold-style-cast"
#pragma clang diagnostic ignored "-Wsign-conversion"


#include "cutils.h"
#include <boost/uuid/sha1.hpp>
#include <boost/endian/conversion.hpp>

using namespace std;

typedef struct {
    std::size_t len;
    uint8_t *data;
} ngx_str_t;

static void ngx_encode_base64_internal(ngx_str_t *dst, ngx_str_t *src, const uint8_t *basis, uintptr_t padding);
static void ngx_encode_base64(ngx_str_t *dst, ngx_str_t *src);

bool derive_sec_websocket_accept(const char* key, size_t len, char* dest) {
    ngx_str_t   encoded, decoded;
    uint32_t digest32[5];
    uint8_t  *digest = (uint8_t*)digest32;

    decoded.len = 20;
    decoded.data = digest;

    boost::uuids::detail::sha1 s;
    s.process_bytes(key, len);
    s.process_bytes("258EAFA5-E914-47DA-95CA-C5AB0DC85B11", 36);
    s.get_digest(digest32);

    for(int i=0; i<5; i++) {
        boost::endian::native_to_big_inplace(digest32[i]);
    }

    encoded.data = (uint8_t *)dest;
    ngx_encode_base64(&encoded, &decoded);

    return true;
}


inline void ngx_encode_base64(ngx_str_t *dst, ngx_str_t *src) {
    static uint8_t basis64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    ngx_encode_base64_internal(dst, src, basis64, 1);
}

static void ngx_encode_base64_internal(ngx_str_t *dst, ngx_str_t *src, const uint8_t *basis, uintptr_t padding) {
    uint8_t *d, *s;
    size_t len;

    len = src->len;
    s = src->data;
    d = dst->data;

    while (len > 2) {
        *d++ = basis[(s[0] >> 2) & 0x3f];
        *d++ = basis[((s[0] & 3) << 4) | (s[1] >> 4)];
        *d++ = basis[((s[1] & 0x0f) << 2) | (s[2] >> 6)];
        *d++ = basis[s[2] & 0x3f];

        s += 3;
        len -= 3;
    }

    if (len) {
        *d++ = basis[(s[0] >> 2) & 0x3f];

        if (len == 1) {
            *d++ = basis[(s[0] & 3) << 4];
            if (padding) {
                *d++ = '=';
            }

        } else {
            *d++ = basis[((s[0] & 3) << 4) | (s[1] >> 4)];
            *d++ = basis[(s[1] & 0x0f) << 2];
        }

        if (padding) {
            *d++ = '=';
        }
    }

    dst->len = d - dst->data;
}

bool utf8_check_is_valid(const boost::string_ref& string)
{
    int c,n,j;
    size_t i,ix;
    for (i=0, ix=string.length(); i < ix; i++)
    {
        c = (unsigned char) string[i];
        //if (c==0x09 || c==0x0a || c==0x0d || (0x20 <= c && c <= 0x7e) ) n = 0; // is_printable_ascii
        if (0x00 <= c && c <= 0x7f) n=0; // 0bbbbbbb
        else if ((c & 0xE0) == 0xC0) n=1; // 110bbbbb
        else if ( c==0xed && i<(ix-1) && ((unsigned char)string[i+1] & 0xa0)==0xa0) return false; //U+d800 to U+dfff
        else if ((c & 0xF0) == 0xE0) n=2; // 1110bbbb
        else if ((c & 0xF8) == 0xF0) n=3; // 11110bbb
            //else if (($c & 0xFC) == 0xF8) n=4; // 111110bb //byte 5, unnecessary in 4 byte UTF-8
            //else if (($c & 0xFE) == 0xFC) n=5; // 1111110b //byte 6, unnecessary in 4 byte UTF-8
        else return false;
        for (j=0; j<n && i<ix; j++) { // n bytes matching 10bbbbbb follow ?
            if ((++i == ix) || (( (unsigned char)string[i] & 0xC0) != 0x80))
                return false;
        }
    }
    return true;
}

#pragma clang diagnostic pop
