#!/bin/sh

LLVM_ROOT=/home/rahul/workspace/llvm
CLANG_C_COMPILER=/usr/bin/clang
CLANG_CXX_COMPILER=/usr/bin/clang++
LLVM_RELEASE_VER=release_38
BOOST_SOURCE=/home/rahul/workspace/boost_1_60_0
GOOGLE_TEST_ROOT=/home/rahul/workspace/gtest

# Clone LLVM
git clone --depth=1 -b ${LLVM_RELEASE_VER} --single-branch git@github.com:llvm-mirror/llvm.git ${LLVM_ROOT}

# Clone Tools
cd ${LLVM_ROOT}/tools
git clone --depth=1 -b ${LLVM_RELEASE_VER} --single-branch git@github.com:llvm-mirror/clang.git
git clone --depth=1 -b ${LLVM_RELEASE_VER} --single-branch git@github.com:llvm-mirror/lldb.git
git clone --depth=1 -b ${LLVM_RELEASE_VER} --single-branch git@github.com:llvm-mirror/lld.git

# Clone Clang Tools
cd ${LLVM_ROOT}/tools/clang/tools
git clone --depth=1 -b ${LLVM_RELEASE_VER} --single-branch git@github.com:llvm-mirror/clang-tools-extra.git

# Clone llvm projects
cd ${LLVM_ROOT}/projects
git clone --depth=1 -b ${LLVM_RELEASE_VER} --single-branch git@github.com:llvm-mirror/compiler-rt.git
git clone --depth=1 -b ${LLVM_RELEASE_VER} --single-branch git@github.com:llvm-mirror/libcxx.git
git clone --depth=1 -b ${LLVM_RELEASE_VER} --single-branch git@github.com:llvm-mirror/libcxxabi.git

# create build directory
mkdir -p ${LLVM_ROOT}/build_local
mkdir -p ${LLVM_ROOT}/install_local

# Build LLVM and CLANG with all toolsi
cd ${LLVM_ROOT}/build_local
cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX=${LLVM_ROOT}/install_local -DCMAKE_C_COMPILER=${CLANG_C_COMPILER} -DCMAKE_CXX_COMPILER=${CLANG_CXX_COMPILER} -DLLVM_TOOL_LLDB_BUILD=ON -DLLVM_TOOL_LLD_BUILD=ON -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DLLVM_INSTALL_UTILS=ON -DLLVM_TOOL_CLANG_TOOLS_EXTRA_BUILD=ON -DCMAKE_BUILD_TYPE=Release -DLLVM_ENABLE_RTTI=ON ${LLVM_ROOT}
make install

# We build this separately to add llvm sanitizers
for SAN_TYPE in NoSan Address Undefined MemoryWithOrigins Thread; do
    if [ "${SAN_TYPE}" = "NoSan" ]; then
        SAN_NAME=""
    else
        SAN_NAME="${SAN_TYPE}"
    fi
    LIB_INSTALL_LOCATION=${LLVM_ROOT}/install_local/libcxx/${SAN_TYPE}
    mkdir -p ${LIB_INSTALL_LOCATION}

    # libcxxabi
    mkdir -p ${LLVM_ROOT}/projects/libcxxabi/build_local
    cd ${LLVM_ROOT}/projects/libcxxabi/build_local

    cmake -DLLVM_USE_SANITIZER=${SAN_NAME} -DCMAKE_INSTALL_PREFIX=${LIB_INSTALL_LOCATION} -DCMAKE_C_COMPILER=${LLVM_ROOT}/install_local/bin/clang -DCMAKE_CXX_COMPILER=${LLVM_ROOT}/install_local/bin/clang++ ${LLVM_ROOT}/projects/libcxxabi/
    make install

    # libcxx
    mkdir -p ${LLVM_ROOT}/projects/libcxx/build_local
    cd ${LLVM_ROOT}/projects/libcxx/build_local

    cmake -DLLVM_USE_SANITIZER=${SAN_NAME} -DLIBCXX_LINK_FLAGS=${LIB_INSTALL_LOCATION}/lib -DLIBCXX_CXX_ABI=libcxxabi -DLIBCXX_CXX_ABI_INCLUDE_PATHS=${LLVM_ROOT}/projects/libcxxabi/include -DCMAKE_INSTALL_PREFIX=${LIB_INSTALL_LOCATION} -DCMAKE_C_COMPILER=${LLVM_ROOT}/install_local/bin/clang -DCMAKE_CXX_COMPILER=${LLVM_ROOT}/install_local/bin/clang++ ${LLVM_ROOT}/projects/libcxx/
    make install
done


cd $BOOST_SOURCE
mkdir -p ${BOOST_SOURCE}/build_local
./bootstrap.sh --with-toolset=clang --prefix=${BOOST_SOURCE}/build_local
./b2 clean
BOOST_CXX_FLAGS="-nostdinc++ -std=c++1y -isystem${LLVM_ROOT}/install_local/libcxx/include -isystem${LLVM_ROOT}/install_local/libcxx/include/c++/v1"
BOOST_LINK_FLAGS="-nostdinc++ -std=c++1y -isystem${LLVM_ROOT}/install_local/libcxx/include -isystem${LLVM_ROOT}/install_local/libcxx/include/c++/v1"
./b2 -q toolset=clang cxxflags="${BOOST_CXX_FLAGS}" linkflags="${BOOST_LINK_FLAGS}" --prefix=${BOOST_SOURCE}/build_local link=static variant=release install
