//
// Created by rahul on 2/28/16.
//

#ifndef CHATTERDB_CONST_H
#define CHATTERDB_CONST_H

#include <stddef.h>

const size_t PAGE_SIZE = 4096;

#endif //CHATTERDB_CONST_H
