//
// Created by rahul on 3/30/16.
//

#ifndef CHATTERDB_HTTPPARSER_H
#define CHATTERDB_HTTPPARSER_H

#include "http_parser.h"
#include <string>
#include <stdexcept>


class ParserException : public std::exception {
    std::string error;
public:
    ParserException(const http_parser& p);
    const char* what() const noexcept;
};

class InvalidData : public std::exception {
public:
    const char* what() const noexcept;
};


class HTTPParser {
public:
    HTTPParser();

    void received_data(const char* data, std::size_t len);

    static int on_message_begin(http_parser *) { return 0; }

    static int on_url(http_parser *parser, const char *at, std::size_t length);

    static int on_status(http_parser *parser, const char *at, std::size_t length);

    static int on_header_field(http_parser *parser, const char *at, std::size_t length);

    static int on_header_value(http_parser *parser, const char *at, std::size_t length);

    static int on_headers_complete(http_parser *parser) {
        (reinterpret_cast<HTTPParser*>(parser->data))->header_complete = true;
        return 0;
    }

    static int on_body(http_parser *, const char *, std::size_t) { throw InvalidData(); }

    static int on_message_complete(http_parser *parser) {
        (reinterpret_cast<HTTPParser*>(parser->data))->message_complete = true;
        return 0;
    }

    static http_parser_settings settings;
    std::string url;
    std::string user_agent;
    std::string upgrade;
    std::string sec_websocket_key;
    std::string sec_websocket_version;
    http_parser parser;
    std::string *current_field;
    std::string current_header;
    bool header_complete;
    bool message_complete;
    bool is_header_state;
};


#endif //CHATTERDB_HTTPPARSER_H
