#include <ctime>
#include <iostream>
#include <list>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/container/set.hpp>

#include "WebSocket.h"
#include "HTTPParser.h"
#include "spdlog/spdlog.h"

using namespace std;
using namespace boost;

//static void rcv_cb(WebSocket &, FrameData::Payload &pl, Opcode , bool ) {
static void rcv_cb(WebSocket &ws, FrameData::Payload &pl, Opcode type, bool is_final) {
//  for (unsigned int i = 0; i < pl.size(); i++) {
//    cout << static_cast<char>(pl[i]);
//  }
//  cout << endl;
  spdlog::get("console")->trace("Received: {}", pl.size());
  ws.send(pl, type, is_final);
}

static void snt_cb(WebSocket &, FrameData::Payload &package) {
  spdlog::get("console")->trace("Sent: {}", package.size());
}

int main() {
  auto console = spdlog::stdout_logger_mt("console", true);
  spdlog::set_level(spdlog::level::err);
  try {
    boost::asio::io_service io_service;

    // Construct a signal set registered for process termination.
    boost::asio::signal_set signals(io_service, SIGINT, SIGTERM);

    // Start an asynchronous wait for one of the signals to occur.
    signals.async_wait([&](
        const boost::system::error_code& ,
        int ) {
      spdlog::get("console")->info("Stopping");
      io_service.stop();
    });

    Acceptor acceptor(io_service, rcv_cb, snt_cb);
    acceptor.start_accept();

    io_service.run();
  }
  catch (std::exception &e) {
    std::cerr << e.what() << std::endl;
    return -1;
  }

  return 0;
}
